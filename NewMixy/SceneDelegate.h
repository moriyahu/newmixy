//
//  SceneDelegate.h
//  NewMixy
//
//  Created by steve on 2019/10/27.
//  Copyright © 2019 steve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

